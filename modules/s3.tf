resource "aws_s3_bucket" "image_bucket" {
  bucket = "iotd-${var.environment}-images"
}

resource "aws_s3_bucket_acl" "image_bucket_acl" {
  bucket = aws_s3_bucket.image_bucket.id
  acl    = "private"
}

# COMMENT conflict with AWS_DEFAULT_ACL in iotd.settings
# resource "aws_s3_bucket_public_access_block" "block" {
#   bucket = aws_s3_bucket.image_bucket.id

#   block_public_acls   = true
#   block_public_policy = true
# }


data "aws_iam_policy_document" "restrict_access" {
  statement {

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    effect = "Deny"

    actions = [
      "s3:*"
    ]

    resources = [
      aws_s3_bucket.image_bucket.arn,
      "${aws_s3_bucket.image_bucket.arn}/*",
    ]

    condition {
      test     = "ForAnyValue:StringNotEquals"
      variable = "aws:sourceVpc"
      values   = ["${module.vpc.vpc_id}"]
    }
  }
}

resource "aws_s3_bucket_policy" "restrict_access" {
  bucket = aws_s3_bucket.image_bucket.id
  policy = data.aws_iam_policy_document.restrict_access.json
}

resource "aws_iam_policy" "upload_policy" {
  name        = "iotd-${var.environment}-upload-policy"
  description = "Policy to upload files from python to S3 bucket"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        "Effect" : "Allow",
        "Action" : "s3:ListAllMyBuckets",
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : ["s3:ListBucket", "s3:GetBucketLocation"],
        "Resource" : aws_s3_bucket.image_bucket.arn
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:PutObject",
          "s3:PutObjectAcl",
          "s3:GetObject",
          "s3:GetObjectAcl",
          "s3:DeleteObject"
        ],
        "Resource" : "${aws_s3_bucket.image_bucket.arn}/*"
      }
    ]
  })
}

resource "aws_vpc_endpoint" "s3" {
  vpc_endpoint_type = "Gateway"
  service_name      = "com.amazonaws.${data.aws_region.current.name}.s3"
  vpc_id            = module.vpc.vpc_id
  route_table_ids   = module.vpc.private_route_table_ids
}
