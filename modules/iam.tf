resource "aws_iam_user" "iotd_user_service" {
  name = "iotd-${var.environment}-user-service"
}

resource "aws_iam_access_key" "iotd_service_key" {
  user = aws_iam_user.iotd_user_service.name
}

resource "aws_iam_user_policy_attachment" "policy-bucket-attach" {
  user       = aws_iam_user.iotd_user_service.name
  policy_arn = aws_iam_policy.upload_policy.arn
}
