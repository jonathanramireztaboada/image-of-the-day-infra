data "aws_region" "current" {}

module "vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  name               = "iotd-${var.environment}-vpc"
  cidr               = "10.10.10.0/24"
  azs                = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  private_subnets    = ["10.10.10.0/27", "10.10.10.32/27", "10.10.10.64/27"]
  public_subnets     = ["10.10.10.96/27", "10.10.10.128/27", "10.10.10.160/27"]
  enable_nat_gateway = true
  single_nat_gateway = true
}
