# Allow ECS Exec commands on tasks
# https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-exec.html
data "aws_iam_policy_document" "ecs_exec_policy" {
  statement {
    sid = "EcsExec"

    actions = [
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "ecs_exec_policy" {
  name   = "iotd-${var.environment}-task-policy"
  policy = data.aws_iam_policy_document.ecs_exec_policy.json
}

resource "aws_iam_role" "app_task_role" {
  name = "iotd-${var.environment}-task-role"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
	{
	  "Sid": "",
	  "Effect": "Allow",
	  "Principal": {
		"Service": "ecs-tasks.amazonaws.com"
	  },
	  "Action": "sts:AssumeRole"
	}
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_exec" {
  role       = aws_iam_role.app_task_role.name
  policy_arn = aws_iam_policy.ecs_exec_policy.arn
}

resource "aws_ecs_task_definition" "iotd_task" {
  family                = "iotd-${var.environment}"
  task_role_arn         = aws_iam_role.app_task_role.arn
  network_mode          = "bridge"
  container_definitions = <<EOF
[{
    "name": "iotd",
    "image": "jonathanramireztaboada/image-of-the-day:latest",
    "command": ["./start.sh"],
    "essential": true,
    "environment": [
            {"name": "DJANGO_SETTINGS_MODULE", "value": "iotd.settings"},
            {"name": "S3_BUCKET_NAME", "value": "${aws_s3_bucket.image_bucket.id}"},
            {"name": "RDS_HOSTNAME", "value": "${aws_db_instance.rds.address}"},
            {"name": "RDS_DB_NAME", "value": "${aws_db_instance.rds.db_name}"},
            {"name": "RDS_PORT", "value": "5432"},
            {"name": "RDS_USERNAME", "value": "postgres"},
            {"name": "RDS_PASSWORD", "value": "${random_password.rds_password.result}"},
            {"name": "AWS_S3_ACCESS_KEY_ID", "value": "${aws_iam_access_key.iotd_service_key.id}"},
            {"name": "AWS_S3_SECRET_ACCESS_KEY", "value": "${aws_iam_access_key.iotd_service_key.secret}"}
        ],
    "portMappings": [
      {
        "containerPort": 8000
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${aws_cloudwatch_log_group.logs.name}",
        "awslogs-stream-prefix": "iotd",
        "awslogs-region": "${data.aws_region.current.name}"
      }
    },
    "memoryReservation": 200
  }
]
EOF
}

resource "aws_ecs_task_definition" "iotd_init_task" {
  family                = "iotd-init-${var.environment}"
  task_role_arn         = aws_iam_role.app_task_role.arn
  network_mode          = "bridge"
  container_definitions = <<EOF
[{
    "name": "iotd-init",
    "image": "jonathanramireztaboada/image-of-the-day:latest",
    "command": ["./init.sh"],
    "environment": [
            {"name": "DJANGO_SETTINGS_MODULE", "value": "iotd.settings"},
            {"name": "S3_BUCKET_NAME", "value": "${aws_s3_bucket.image_bucket.id}"},
            {"name": "RDS_HOSTNAME", "value": "${aws_db_instance.rds.address}"},
            {"name": "RDS_DB_NAME", "value": "${aws_db_instance.rds.db_name}"},
            {"name": "RDS_PORT", "value": "5432"},
            {"name": "RDS_USERNAME", "value": "postgres"},
            {"name": "RDS_PASSWORD", "value": "${random_password.rds_password.result}"}
        ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${aws_cloudwatch_log_group.logs.name}",
        "awslogs-stream-prefix": "iotd",
        "awslogs-region": "${data.aws_region.current.name}"
      }
    },
    "memoryReservation": 200
  }
]
EOF
}

resource "aws_ecs_service" "iotd_service" {
  name                               = "iotd-${var.environment}-service"
  cluster                            = aws_ecs_cluster.ecs.id
  task_definition                    = aws_ecs_task_definition.iotd_task.arn
  desired_count                      = 2
  deployment_minimum_healthy_percent = 50


  ordered_placement_strategy {
    type  = "spread"
    field = "host"
  }

  load_balancer {
    target_group_arn = module.alb.target_group_arns[0]
    container_name   = "iotd"
    container_port   = "8000"
  }

  lifecycle {
    ignore_changes = [desired_count]
  }
}
