variable "environment" {
  type        = string
  description = "Environment name"
}

variable "default_tags" {
  default     = {}
  description = "Default Tags for Auto Scaling Group"
  type        = map(string)
}

variable "instance_type" {
  description = "EC2 instance type (default=t3.micro)"
  default     = "t3.micro"
}

variable "asg_max_size" {
  description = "Maximum number EC2 instances"
  default     = 2
}

variable "asg_min_size" {
  description = "Minimum number EC2 instances"
  default     = 1
}

variable "asg_desired_size" {
  description = "Desired number EC2 instances"
  default     = 1
}
