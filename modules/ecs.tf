resource "aws_ecs_cluster" "ecs" {
  name = "iotd-${var.environment}-cluster"
}

data "aws_ami" "ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-*-amazon-ecs-optimized"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}

resource "aws_cloudwatch_log_group" "logs" {
  name              = "iotd-${var.environment}-log-group"
  retention_in_days = 3
}

data "aws_iam_policy_document" "instance_policy" {
  statement {
    sid = "CloudwatchPutMetricData"

    actions = [
      "cloudwatch:PutMetricData",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    sid = "InstanceLogging"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
    ]

    resources = [
      aws_cloudwatch_log_group.logs.arn,
    ]
  }

  statement {
    sid = "NameItself"

    actions = [
      "ec2:CreateTags"
    ]

    resources = ["*"]

    condition {
      test     = "StringEquals"
      values   = ["&{ec2:SourceInstanceARN}"]
      variable = "aws:ARN"
    }

    condition {
      test     = "ForAllValues:StringEquals"
      values   = ["Name"]
      variable = "aws:TagKeys"
    }
  }
}

resource "aws_iam_policy" "instance_policy" {
  name   = "iotd-${var.environment}-ecs-instance-policy"
  path   = "/"
  policy = data.aws_iam_policy_document.instance_policy.json
}

resource "aws_iam_role" "instance_role" {
  name = "iotd-${var.environment}-instance-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "application-autoscaling.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_policy" {
  role       = aws_iam_role.instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "instance_policy" {
  role       = aws_iam_role.instance_role.name
  policy_arn = aws_iam_policy.instance_policy.arn
}

resource "aws_iam_instance_profile" "instance" {
  name = "iotd-${var.environment}-instance-profile"
  role = aws_iam_role.instance_role.name
}

resource "aws_security_group" "ecs_security_group" {
  name   = "iotd-${var.environment}-ecs-security-group"
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "instance_out_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecs_security_group.id
}

data "template_file" "user_data" {
  template = file("${path.module}/user_data.sh")

  vars = {
    ecs_cluster                 = aws_ecs_cluster.ecs.name
    log_group                   = aws_cloudwatch_log_group.logs.name
    additional_user_data_script = ""
  }
}

resource "aws_launch_template" "template_instance" {
  name_prefix            = "iotd-${var.environment}-template"
  image_id               = data.aws_ami.ami.id
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.ecs_security_group.id]
  user_data              = base64encode(data.template_file.user_data.rendered)
  update_default_version = true

  iam_instance_profile {
    name = aws_iam_instance_profile.instance.name
  }

  block_device_mappings {
    device_name = "/dev/xvda" # root volume

    ebs {
      volume_size = 10
      volume_type = "gp3"
    }
  }

  credit_specification {
    cpu_credits = "standard"
  }
}

resource "aws_autoscaling_group" "asg" {
  name = "iotd-${var.environment}-asg"

  vpc_zone_identifier = module.vpc.private_subnets
  max_size            = var.asg_max_size
  min_size            = var.asg_min_size
  desired_capacity    = var.asg_desired_size

  launch_template {
    id = aws_launch_template.template_instance.id
  }

  health_check_grace_period = 300
  health_check_type         = "EC2"

  dynamic "tag" {
    for_each = var.default_tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}
