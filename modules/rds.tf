resource "random_password" "rds_password" {
  length           = 16
  special          = true
  override_special = "-_$#!&()[]<>"
}

resource "aws_security_group" "rds" {
  name   = "iotd-rds-${var.environment}-sg"
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "rds_in_ec2" {
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.ecs_security_group.id
  security_group_id        = aws_security_group.rds.id
}

resource "aws_db_subnet_group" "rds_subnet" {
  name       = "iotd-${var.environment}-rds-subnet"
  subnet_ids = module.vpc.private_subnets
}

resource "aws_db_instance" "rds" {
  db_name                = "iotd${var.environment}rds"
  allocated_storage      = 20
  engine                 = "postgres"
  engine_version         = "14.2"
  instance_class         = "db.t3.micro"
  username               = "postgres"
  password               = random_password.rds_password.result
  publicly_accessible    = false
  db_subnet_group_name   = aws_db_subnet_group.rds_subnet.name
  vpc_security_group_ids = [aws_security_group.rds.id]
}
