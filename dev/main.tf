terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
  backend "s3" {
    bucket = "tfstates-image-of-the-day"
    key    = "image-of-the-day-dev.terraform.tfstate"
    region = "eu-west-1"
  }
}

locals {
  default_tags = {
    Environment = "dev"
    Project     = "iotd"
  }
}

provider "aws" {
  region = "eu-west-1"
  default_tags {
    tags = local.default_tags
  }
}

module "infra" {
  source       = "../modules"
  environment  = "dev"
  default_tags = local.default_tags
}
